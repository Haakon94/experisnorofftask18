﻿namespace Task18Noroff {
    partial class UpdateForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.updateOk = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.nameEditInput = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // updateOk
            // 
            this.updateOk.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.updateOk.Location = new System.Drawing.Point(17, 113);
            this.updateOk.Name = "updateOk";
            this.updateOk.Size = new System.Drawing.Size(110, 32);
            this.updateOk.TabIndex = 0;
            this.updateOk.Text = "OK";
            this.updateOk.UseVisualStyleBackColor = true;
            this.updateOk.Click += new System.EventHandler(this.UpdateOk_Click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(172, 113);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(110, 32);
            this.button2.TabIndex = 1;
            this.button2.Text = "CANCEL";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // nameEditInput
            // 
            this.nameEditInput.Location = new System.Drawing.Point(17, 59);
            this.nameEditInput.Name = "nameEditInput";
            this.nameEditInput.Size = new System.Drawing.Size(265, 22);
            this.nameEditInput.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(129, 25);
            this.label1.TabIndex = 4;
            this.label1.Text = "Update name";
            // 
            // UpdateForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(337, 212);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.nameEditInput);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.updateOk);
            this.Name = "UpdateForm";
            this.Text = "UpdateForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button updateOk;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox nameEditInput;
        private System.Windows.Forms.Label label1;
    }
}