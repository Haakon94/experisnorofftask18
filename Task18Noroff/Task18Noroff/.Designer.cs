﻿namespace Task18Noroff {
    partial class rpgUI {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.NameInput = new System.Windows.Forms.TextBox();
            this.CharacterList = new System.Windows.Forms.ComboBox();
            this.InsertToDb = new System.Windows.Forms.Button();
            this.UpdateBtn = new System.Windows.Forms.Button();
            this.DbList = new System.Windows.Forms.ListBox();
            this.DeleteBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtGenderBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // NameInput
            // 
            this.NameInput.Location = new System.Drawing.Point(12, 59);
            this.NameInput.Name = "NameInput";
            this.NameInput.Size = new System.Drawing.Size(264, 22);
            this.NameInput.TabIndex = 0;
            // 
            // CharacterList
            // 
            this.CharacterList.FormattingEnabled = true;
            this.CharacterList.Location = new System.Drawing.Point(12, 203);
            this.CharacterList.Name = "CharacterList";
            this.CharacterList.Size = new System.Drawing.Size(264, 24);
            this.CharacterList.TabIndex = 2;
            // 
            // InsertToDb
            // 
            this.InsertToDb.Location = new System.Drawing.Point(12, 261);
            this.InsertToDb.Name = "InsertToDb";
            this.InsertToDb.Size = new System.Drawing.Size(264, 41);
            this.InsertToDb.TabIndex = 4;
            this.InsertToDb.Text = "Insert";
            this.InsertToDb.UseVisualStyleBackColor = true;
            this.InsertToDb.Click += new System.EventHandler(this.InsertToDb_Click);
            // 
            // UpdateBtn
            // 
            this.UpdateBtn.Location = new System.Drawing.Point(378, 383);
            this.UpdateBtn.Name = "UpdateBtn";
            this.UpdateBtn.Size = new System.Drawing.Size(116, 42);
            this.UpdateBtn.TabIndex = 5;
            this.UpdateBtn.Text = "Update";
            this.UpdateBtn.UseVisualStyleBackColor = true;
            this.UpdateBtn.Click += new System.EventHandler(this.UpdateBtn_Click);
            // 
            // DbList
            // 
            this.DbList.FormattingEnabled = true;
            this.DbList.ItemHeight = 16;
            this.DbList.Location = new System.Drawing.Point(378, 59);
            this.DbList.Name = "DbList";
            this.DbList.Size = new System.Drawing.Size(456, 308);
            this.DbList.TabIndex = 7;
            // 
            // DeleteBtn
            // 
            this.DeleteBtn.Location = new System.Drawing.Point(515, 383);
            this.DeleteBtn.Name = "DeleteBtn";
            this.DeleteBtn.Size = new System.Drawing.Size(125, 42);
            this.DeleteBtn.TabIndex = 8;
            this.DeleteBtn.Text = "Delete";
            this.DeleteBtn.UseVisualStyleBackColor = true;
            this.DeleteBtn.Click += new System.EventHandler(this.DeleteBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(7, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 25);
            this.label1.TabIndex = 9;
            this.label1.Text = "Insert name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(7, 175);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(167, 25);
            this.label2.TabIndex = 10;
            this.label2.Text = "Choose character";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(373, 27);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(399, 29);
            this.label4.TabIndex = 12;
            this.label4.Text = "Character overview from database";
            // 
            // txtGenderBox
            // 
            this.txtGenderBox.Location = new System.Drawing.Point(12, 132);
            this.txtGenderBox.Name = "txtGenderBox";
            this.txtGenderBox.Size = new System.Drawing.Size(264, 22);
            this.txtGenderBox.TabIndex = 13;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(7, 104);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(126, 25);
            this.label3.TabIndex = 14;
            this.label3.Text = "Insert gender";
            // 
            // rpgUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(860, 478);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtGenderBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.DeleteBtn);
            this.Controls.Add(this.DbList);
            this.Controls.Add(this.UpdateBtn);
            this.Controls.Add(this.InsertToDb);
            this.Controls.Add(this.CharacterList);
            this.Controls.Add(this.NameInput);
            this.Name = "rpgUI";
            this.Text = "Task 18";
            this.Load += new System.EventHandler(this.RpgUI_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox NameInput;
        private System.Windows.Forms.ComboBox CharacterList;
        private System.Windows.Forms.Button InsertToDb;
        private System.Windows.Forms.Button UpdateBtn;
        private System.Windows.Forms.ListBox DbList;
        private System.Windows.Forms.Button DeleteBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtGenderBox;
        private System.Windows.Forms.Label label3;
    }
}

