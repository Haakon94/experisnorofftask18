﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NoroffTask17Part1;
using System.Data.SQLite;

namespace Task18Noroff {
    public partial class rpgUI : Form {

        #region Creates connection
        //Creates the connection to a specified database, rpgDatabase is put in Bin/Debug
        private static SQLiteConnection conn = new SQLiteConnection(
            "DataSource = rpgDatabase.db; Version = 3; New = True; Compress = True; ");
        #endregion

        public rpgUI() {
            InitializeComponent();
        }
        private void RpgUI_Load(object sender, EventArgs e) {
            CharacterList.Items.Add("Wizard");
            CharacterList.Items.Add("Thief");
            CharacterList.Items.Add("Warrior");

            ReadFromDatabase();
        }

        //Method for reading data from database
        public void ReadFromDatabase() {
            try {
                conn.Open();

                SQLiteCommand sqlLite_command = conn.CreateCommand();
                sqlLite_command.CommandText = "SELECT * FROM Character";

                SQLiteDataReader reader = sqlLite_command.ExecuteReader();
                
                while (reader.Read()) {

                    int id = reader.GetInt32(0);
                    string name = reader.GetString(1);
                    string gender = reader.GetString(2);
                    string type = reader.GetString(3);

                    if(type == "Wizard") {
                        // Sets name to display in DbList and sets id as valuemember for access later
                        DbList.DisplayMember = "name";
                        DbList.ValueMember = "id";
                        DbList.Items.Add(new Wizard {
                            Id = id,
                            Name = name,
                            Gender = gender
                        });
                    } else if(type == "Thief") {
                        // Sets name to display in DbList and sets id as valuemember for access later
                        DbList.DisplayMember = "name";
                        DbList.ValueMember = "id";
                        DbList.Items.Add(new Thief {
                            Id = id,
                            Name = name,
                            Gender = gender
                        });
                    } else if(type == "Warrior") {
                        // Sets name to display in DbList and sets id as valuemember for access later
                        DbList.DisplayMember = "name";
                        DbList.ValueMember = "id";
                        DbList.Items.Add(new Warrior {
                            Id = id,
                            Name = name,
                            Gender = gender
                        });
                    }
                }

                conn.Close();
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
        }

        //Method for inserting data to the database
        public void InsertData(SQLiteConnection conn, RPGCharacter rpgchar, string type) {
            try {
                SQLiteCommand sqlite_cmd;
                sqlite_cmd = conn.CreateCommand();
                sqlite_cmd.CommandText = $"INSERT INTO Character (Name,Gender,Type) VALUES('{rpgchar.Name}','{rpgchar.Gender}','{type}');";
                sqlite_cmd.ExecuteNonQuery();   
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
        }

        //Method for deleting data 
        public void DeleteCharacterFromDb(SQLiteConnection conn, int id) {
            try {
                SQLiteCommand sqlite_cmd;
                sqlite_cmd = conn.CreateCommand();
                sqlite_cmd.CommandText = $"DELETE FROM Character WHERE id= '{id}'";
                sqlite_cmd.ExecuteNonQuery();
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
        }

        // Method for updating data
        public void UpdateData(SQLiteConnection conn, string name, int id) {
            try {
                SQLiteCommand sqlite_cmd;
                sqlite_cmd = conn.CreateCommand();
                sqlite_cmd.CommandText = $"UPDATE Character SET name='{name}' WHERE id='{id}';";
                sqlite_cmd.ExecuteNonQuery();
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
        }

        // When insert btn is clicked perform this method and calls insertdata method 
        private void InsertToDb_Click(object sender, EventArgs e) {
            try {
                conn.Open();

                // Takes in values from input fields
                string name = NameInput.Text;
                string gender = txtGenderBox.Text;
                string type = CharacterList.Text;

                SQLiteCommand sqlite_command2 = conn.CreateCommand();
                sqlite_command2.CommandText = "SELECT id FROM Character";
                SQLiteDataReader reader = sqlite_command2.ExecuteReader();
                int id = 0;
                while (reader.Read()) {
                    id = reader.GetInt32(0) + 1; 
                }
                Console.WriteLine(id);
                if (type == "Wizard") {
                    InsertData(conn, new Wizard(id, name, gender), type);
                } else if (type == "Thief") {
                    InsertData(conn, new Thief(id, name, gender), type);
                } else if (type == "Warrior") {
                    InsertData(conn, new Warrior(id, name, gender), type);
                }

                conn.Close();


                DbList.Items.Clear();
                ReadFromDatabase();
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
        }

        // When update btn is clicked perform this method and calls UpdateData method 
        private void UpdateBtn_Click(object sender, EventArgs e) {
            try {
                conn.Open();

                UpdateForm updateForm = new UpdateForm();

                if (updateForm.ShowDialog() == DialogResult.OK) {
                    string updateName = updateForm.GetUpdateName();
                    var characterSelected = ((RPGCharacter)DbList.SelectedItem).Id;

                    UpdateData(conn, updateName, characterSelected);

                    conn.Close();

                    DbList.Items.Clear();
                    ReadFromDatabase();
                }
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
        }

        // When delete btn is clicked perform this method and calls DeleteCharacterFromDb method 
        private void DeleteBtn_Click(object sender, EventArgs e) {
            try {
                conn.Open();
                var characterSelected = ((RPGCharacter)DbList.SelectedItem).Id;

                DeleteCharacterFromDb(conn, characterSelected);
                conn.Close();

                DbList.Items.Clear();
                ReadFromDatabase();
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
